;;; emacs-rc-forth.el --- 

;; Copyright (C) 2016 Gleb Semenov
;;
;; Author: gleb.semenov@gmail.com
;; Keywords: forth gforth gforth.el
;; Requirements: gforth package is installed
;; Status: not intended to be distributed yet

(autoload 'forth-mode "gforth.el")
(autoload 'forth-block-mode "gforth.el")

(setq auto-mode-alist (cons '("\\.fs\\'" . forth-mode) auto-mode-alist))

(setq auto-mode-alist (cons '("\\.fb\\'" . forth-block-mode) auto-mode-alist))

(add-hook 'forth-mode-hook (function (lambda ()
		 ;; customize variables here:
		 (setq forth-indent-level 4)
		 (setq forth-minor-indent-level 2)
		 (setq forth-hilight-level 3))))









