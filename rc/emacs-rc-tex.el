;;; emacs-rc-tex.el --- 

;; Copyright (C) 2003 Alex Ott
;;
;; Author: alexott@gmail.com
;; Keywords: 
;; Requirements: 
;; Status: not intended to be distributed yet


(load "auctex.el" nil t t)
(load "preview-latex.el" nil t t)
(require 'tex-site)

(setq-default TeX-master nil)
(setq TeX-parse-self t)
(setq TeX-auto-save t)
(setq TeX-default-mode 'latex-mode)
(setq TeX-open-quote "``")
(setq TeX-close-quote "''")

(autoload 'turn-on-bib-cite "bib-cite")

(defun My-TeX-keymap ()
  (local-set-key [(meta i)] 
                 '(lambda ()
                    (interactive)
                    (insert "\n\\item "))))

(add-hook 'texinfo-mode-hook
					'(lambda ()
						 (local-set-key [delete]  'delete-char)
						 (setq delete-key-deletes-forward t)
						 ))

(add-hook 'TeX-mode-hook
          '(lambda ()
             (local-set-key "\\" 'TeX-electric-macro)
						 (turn-on-bib-cite)
						 (My-TeX-keymap)
						 (setq bib-cite-use-reftex-view-crossref t)
						 ))

(add-hook 'LaTeX-mode-hook 'TeX-PDF-mode)

;;; emacs-rc-tex.el ends here
