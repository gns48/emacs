;;-- emacs-rc-decor.el

(require 'tree-widget)
(require 'mwheel)
(mwheel-install)

(blink-cursor-mode t)
(setq-default cursor-type '(hbar . 3))

(put 'downcase-region 'disabled nil)
(put 'upcase-region 'disabled nil)

;;-- emacs-rc-decor.el ends here
