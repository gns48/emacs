;;; emacs-rc-ccmode.el --- 

;; Copyright (C) 2003 Alex Ott
;;
;; Author: alexott@gmail.com
;; Keywords: 
;; Requirements: 
;; Status: not intended to be distributed yet

(require 'cc-mode)

;; customisation of cc-mode

(defun my-c-mode-common-hook ()
	;; style customization	
	(c-set-style "linux")
	(setq c-basic-offset 4)
	(setq tab-width 4
				;; this will make sure spaces are used instead of tabs
				;; indent-tabs-mode t
				)
	(c-toggle-auto-hungry-state 0)
	;; minor modes
	(auto-fill-mode 1)
	(hs-minor-mode 1)
	;; local keys
	(local-set-key [delete] 'delete-char)
	(local-set-key [return] 'newline-and-indent)
	(define-key c-mode-base-map "\C-m" 'newline-and-indent)
	(local-set-key "\C-c:" 'uncomment-region)
	(local-set-key "\C-c;" 'comment-region)
	(ggtags-mode 1))

(add-hook 'c-mode-common-hook 'my-c-mode-common-hook)

(defun fp-c-mode-routine ()
  (local-set-key "\M-q" 'rebox-comment))

(add-hook 'c-mode-hook 'fp-c-mode-routine)
;;(add-hook 'c-mode-hook 'semantic-mode)

(autoload 'rebox-comment "rebox" nil t)
(autoload 'rebox-region "rebox" nil t)
(add-to-list 'auto-mode-alist '("\\.ipp?$" . c++-mode))

;;; emacs-rc-cmode.el ends here









