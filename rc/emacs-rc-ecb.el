;;-- emacs-rc-ecb.el

(require 'cl)
(require 'ecb)
(require 'semantic/sb)

;;(require 'ecb-autoloads)

(add-hook 'ecb-before-activate-hook
					(lambda () (semantic-mode 1)))

;;-- cedet
(global-ede-mode 1)

(global-semantic-idle-completions-mode t)
(global-semantic-decoration-mode t)
(global-semantic-highlight-func-mode t)
(global-semantic-show-unmatched-syntax-mode t)

;;-- emacs-rc-ecb.el ends here
