;;; emacs-rc-org-mode.el --- 

;; Copyright (C) 2004 Alex Ott
;;
;; Author: alexott@gmail.com
;; Keywords: 
;; Requirements: 
;; Status: not intended to be distributed yet

;;(add-to-list 'load-path "~/emacs/org-mode")
(require 'org-install)

(add-to-list 'auto-mode-alist '("\\.org$" . org-mode))
(add-to-list 'file-coding-system-alist (cons "\\.org$"  'utf-8))

(define-key global-map "\C-cl" 'org-store-link)
(define-key global-map "\C-ca" 'org-agenda)
(add-hook 'org-mode-hook 'turn-on-font-lock)

;;; emacs-rc-org-mode.el ends here






