;;-- emacs-rc-info.el

(require 'info)
(require 'info-look)

(add-to-list  'Info-directory-list "/usr/share/info")
(add-to-list  'Info-directory-list "/usr/share/info/emacs")
(add-to-list  'Info-directory-list "/usr/local/info")
(add-to-list  'Info-directory-list "/usr/local/share/info")

;;-- emacs-rc-info.el ends here
