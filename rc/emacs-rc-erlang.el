;;; emacs-rc-erlang.el --- 

;; Copyright (C) 2008 Alex Ott
;;
;; Author: alexott@gmail.com
(add-to-list 'load-path
	     (car (file-expand-wildcards "/usr/lib/erlang/lib/tools-*/emacs")))

(require 'erlang-start)

(add-to-list 'auto-mode-alist '("\\.erl?$" . erlang-mode))
(add-to-list 'auto-mode-alist '("\\.hrl?$" . erlang-mode))

;; distel setup
;(add-to-list 'load-path "~/.emacs.d/share/distel/elisp")
;(require 'distel)
;(distel-setup)

;; flymake for erlang
;; doesn't work yet
;;(defun flymake-erlang-init ()
;;  (let* ((temp-file (flymake-init-create-temp-buffer-copy
;;		      'flymake-create-temp-inplace))
;;	 (local-file (file-relative-name 
;;		       temp-file
;;		       (file-name-directory buffer-file-name))))
;;  (list "~/emacs/bin/eflymake" (list local-file))))

;;(add-to-list 'flymake-allowed-file-name-masks
;;	     '("\\.erl\\'" flymake-erlang-init))

(defun my-erlang-mode-hook ()
  ;; when starting an Erlang shell in Emacs, default in the node name
  (setq inferior-erlang-machine-options '("-sname" "emacs"))
  ;; enable flymake
  (flymake-mode 1)
  ;; add Erlang functions to an imenu menu
  (imenu-add-to-menubar "imenu"))
;; Some Erlang customizations
(add-hook 'erlang-mode-hook 'my-erlang-mode-hook)

;; A number of the erlang-extended-mode key bindings are useful in the shell too
(defconst distel-shell-keys
  '(("\C-\M-i"   erl-complete)
    ("\M-?"      erl-complete)	
    ("\M-."      erl-find-source-under-point)
    ("\M-,"      erl-find-source-unwind) 
    ("\M-*"      erl-find-source-unwind) 
    )
  "Additional keys to bind when in Erlang shell.")

(add-hook 'erlang-shell-mode-hook
	  (lambda ()
	    ;; add some Distel bindings to the Erlang shell
	    (dolist (spec distel-shell-keys)
	      (define-key erlang-shell-mode-map (car spec) (cadr spec)))))

;;; emacs-rc-erlang.el ends here



