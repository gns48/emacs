;;; emacs-rc-desktop.el --- Load desktop settings

;; Copyright (C) 2003 Alex Ott
;;
;; Author: alexott@gmail.com
;; Keywords: 
;; Requirements: 
;; Status: not intended to be distributed yet

(add-to-list 'desktop-locals-to-save 'buffer-file-coding-system)
(add-to-list 'desktop-locals-to-save 'tab-width)

(setq-default desktop-missing-file-warning nil)
(setq-default desktop-path (quote ("~/.emacs.d")))
(desktop-save-mode 1)

;;; emacs-rc-desktop.el ends here










