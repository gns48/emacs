;;; emacs-rc-kbd.el --- 

;; Copyright (C) 2003 Alex Ott
;;
;; Author: alexott@gmail.com
;; Keywords: 
;; Requirements: 
;; Status: not intended to be distributed yet

(global-set-key [button5]  'scroll-up)
(global-set-key [button4]  'scroll-down)
(global-set-key [help] 'info)
(global-set-key [f5] 'delete-other-windows)
(global-set-key [f6] 'bury-buffer)
(global-set-key [f8] 'next-error)
(global-set-key [f9] 'My-Compile)
(global-set-key "\M-0" 'buffer-menu)

;; popup menus
(global-set-key [(mouse-3)] 'mouse-major-mode-menu)
(global-set-key [(shift mouse-3)] 'mouse-buffer-menu)

;;; emacs-rc-kbd.el ends here
